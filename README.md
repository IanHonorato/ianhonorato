# Mix Internet - Teste de conhecimento - Vaga de estágio

Este repositório é destinado ao teste de conhecimento para os candidados a vaga de estágio em desenvolvimento da Mix Internet.

## Sobre o teste:
O teste consiste no desenvolvimento de uma pequena página de uma empresa fictícia chamada Mix Viagens.
Esta página contem um pequeno formulário, onde o cliente pode usar para realizar a reserva de uma viagem. 
Esse formulário deve fazer o envio dos dados preenchidos por e-mail.
## Instruções para o teste:

- Faça um fork deste projeto (crie uma conta no Bitbucket, caso não tenha).
*Obs.: O repositorio criado deve ser público.*

- No diretório *layout* existe um arquivo XD com o layout da página que deve ser desenvolvida. *(Acesse esse [link](https://xd.adobe.com/view/5666ab87-43c1-4a0e-94d5-ee29983a3e4c-0335/) para visualizar o layout pelo navegador)*

- Os assets necessários para o desenvolvimento estão no diretório *layout/assets*

- O botão "Reservar" nos cads de promoção devem levar o usuário para o formulário de reserva, no inicio da página.

- Utilize obrigatoriamente as tecnologias:
    - **HTML**;
    - **CSS** (pode utilizar SASS/SCSS);
    - **PHP** (para o envio do formulário por email)

- Caso queira, você também pode utilizar:
    - **Javascript e suas bibliotecas**;
    - **bibliotecas CSS** (recomendo utilizar bootstrap)
    - **bibliotecas PHP** (recomendo utilizar o [PHPMailer](https://php.com.br/51?como-enviar-e-mails-com-php-mailer), para envio dos e-mails);

- Após finalizado o projeto, envie o link do repositório, juntamente com seu nome completo, para o e-mail [victor@mixinternet.com.br](mailto:victor@mixinternet.com.br), com o título: **Teste de conhecimento**.